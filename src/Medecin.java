/**
 * 
 * @author User-06
 *
 */
public class Medecin extends Personne {

	long numAgreement;
	/**
	 * 
	 * @param nom
	 * @param prenom
	 * @param nomRue
	 * @param numRue
	 * @param numLogement
	 * @param codePostal
	 * @param ville
	 * @param phone
	 * @param mail
	 * @param numAgreement
	 */
	Medecin(String nom, String prenom, String nomRue, int numRue, long numLogement, long codePostal, String ville, long phone,
			String mail, long numAgreement) {
		super(nom, prenom, nomRue, numRue, numLogement, codePostal, ville, phone, mail);
		this.numAgreement = numAgreement;
	}
	/**
	 * 
	 * @return
	 */
	public long getNumAgreement() {
		return numAgreement;
	}
	/**
	 * 
	 * @param numAgreement
	 */
	public void setNumAgreement(long numAgreement) {
		this.numAgreement = numAgreement;
	}
	/**
	 * 
	 */
	public String toString() {
		return "Dr. " + this.getNom() + " " + this.getPrenom();
	}
}


