
import java.util.ArrayList;
/**
 * 
 * @author User-06
 *
 */
public class LesListes {

	// liste pour les clients

	static ArrayList <Client> LesClients = new ArrayList<>();
/**
 * 
 * @param client
 */
	public static void addClient(Client client) {
		LesClients.add(client);
	}
/**
 * 
 * @return
 */
	public static ArrayList <Client> getClient () {
		return LesClients;

	}


	// Liste pour les medecins traitants

	static ArrayList <Medecin> lesMedecinsTraitants = new ArrayList<>();
	/**
	 * 
	 * @param medecin
	 */
	public static void addMedecinTraitant (Medecin medecin) {
		lesMedecinsTraitants.add(medecin);
	}
	/**
	 * 
	 * @return
	 */
	public static ArrayList<Medecin> getMedecin(){
		return lesMedecinsTraitants;
	}



	// Liste pour les médicaments

	static ArrayList<Medicament> lesMedicaments = new ArrayList<>();
	/**
	 * 
	 * @param medicament
	 */
	public static void addMedicaments (Medicament medicament) {
		lesMedicaments.add(medicament);
	}
	/**
	 * 
	 * @return
	 */
	public static ArrayList<Medicament> getMedicament(){
		return lesMedicaments;
	}


	// Liste pour les spécialistes

	static ArrayList<Specialiste> lesSpecialistes = new ArrayList<>();
	/**
	 * 
	 * @param specialiste
	 */
	public static void addSpecialistes(Specialiste specialiste) {
		lesSpecialistes.add(specialiste);
	}
	/**
	 * 
	 * @return
	 */
	public static ArrayList<Specialiste> getSpecialiste(){
		return lesSpecialistes;
	}



	// Liste pour Mutuelle

	static ArrayList<Mutuelle> lesMutuelles= new ArrayList<>();
	/**
	 * 
	 * @param mutuelleSante
	 */
	public static void addMutuelles(Mutuelle mutuelleSante) {
		lesMutuelles.add(mutuelleSante);
	}
	/**
	 * 
	 * @return
	 */
	public static ArrayList<Mutuelle> getMutuelle(){
		return lesMutuelles;
	}

	// Liste pour les ordonnances

	static ArrayList<Ordonnance> lesOrdonnances = new ArrayList<>();
	/**
	 * 
	 * @param ordonnance
	 */
	public static void addOrdonnances(Ordonnance ordonnance) {
		lesOrdonnances.add(ordonnance);
	}
	/**
	 * 
	 * @return
	 */
	public static ArrayList<Ordonnance> getOrdonnance(){
		return lesOrdonnances;
	}
}