/**
 * 
 * @author User-06
 *
 */
public abstract class Personne {

	private String nom;
	private String prenom;
	private String nomRue;
	private int numRue;
	private long numLogement;
	private long codePostal;
	private String ville;
	private long phone;
	private String mail;
	/**
	 * 
	 * @param nom
	 * @param prenom
	 * @param nomRue
	 * @param numRue
	 * @param numLogement
	 * @param codePostal
	 * @param ville
	 * @param phone
	 * @param mail
	 */
	Personne(String nom, String prenom, String nomRue, int numRue, long numLogement, long codePostal, String ville, long phone, String mail){
		this.nom = nom.toUpperCase();
		this.prenom = prenom;
		this.nomRue = nomRue;
		this.numRue = numRue;
		this.numLogement = numLogement;
		this.codePostal = codePostal;
		this.ville = ville;
		this.phone = phone;
		this.mail = mail;

	}
	/**
	 * 
	 */
	Personne(){	
	}
	/**
	 * 
	 * @return
	 */
	public String getNom() {
		return nom;
	}
	/**
	 * 
	 * @return
	 */
	public String getPrenom() {
		return prenom;
	}
	/**
	 * 
	 * @return
	 */
	public String getNomRue() {
		return nomRue;
	}
	/**
	 * 
	 * @return
	 */
	public int getNumRue() {
		return numRue;
	}
	/**
	 * 
	 * @return
	 */
	public long getNumLogement() {
		return numLogement;
	}
	/**
	 * 
	 * @return
	 */
	public long getCodePostal() {
		return codePostal;
	}
	/**
	 * 
	 * @return
	 */
	public String getVille() {
		return ville;
	}
	/**
	 * 
	 * @return
	 */
	public long getPhone() {
		return phone;
	}
	/**
	 * 
	 * @return
	 */
	public String getMail() {
		return mail;
	}
	/**
	 * 
	 * @param nom
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}
	/**
	 * 
	 * @param prenom
	 */
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	/**
	 * 
	 * @param nomRue
	 */
	public void setNomRue(String nomRue) {
		this.nomRue = nomRue;
	}
	/**
	 * 
	 * @param numRue
	 */
	public void setNumRue(int numRue) {
		this.numRue = numRue;
	}
	/**
	 * 
	 * @param numLogement
	 */
	public void setNumLogement(int numLogement) {
		this.numLogement = numLogement;
	}
	/**
	 * 
	 * @param codePostal
	 */
	public void setCodePostal(int codePostal) {
		this.codePostal = codePostal;
	}
	/**
	 * 
	 * @param ville
	 */
	public void setVille(String ville) {
		this.ville = ville;
	}
	/**
	 * 
	 * @param phone
	 */
	public void setPhone(long phone) {
		this.phone = phone;
	}
	/**
	 * 
	 * @param mail
	 */
	public void setMail(String mail) {
		this.mail = mail;
	}
}


