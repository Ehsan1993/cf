/**
 * 
 * @author User-06
 *
 */
public class Medicament {

	String nom;
	String categorie;
	int prix;
	Dates dateService;
	int quantite;
	/**
	 * 
	 * @param nom
	 * @param categorie
	 * @param prix
	 * @param dateService
	 * @param quantite
	 */
	Medicament(String nom, String categorie, int prix, Dates dateService, int quantite){

		this.nom = nom;
		this.categorie = categorie;
		this.prix = prix;
		this.dateService = dateService;
		this.quantite = quantite;

	}
	/**
	 * 
	 * @return
	 */
	public String getNom() {
		return nom;
	}
	/**
	 * 
	 * @param nom
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}
	/**
	 * 
	 * @return
	 */
	public String getCategorie() {
		return categorie;
	}
	/**
	 * 
	 * @param categorie
	 */
	public void setCategorie(String categorie) {
		this.categorie = categorie;
	}
	/**
	 * 
	 * @return
	 */
	public int getPrix() {
		return prix;
	}
	/**
	 * 
	 * @param prix
	 */
	public void setPrix(int prix) {
		this.prix = prix;
	}
	/**
	 * 
	 * @return
	 */
	public Dates getDateService() {
		return dateService;
	}
	/**
	 * 
	 * @param dateService
	 */
	public void setDateService(Dates dateService) {
		this.dateService = dateService;
	}
	/**
	 * 
	 * @return
	 */
	public int getQuantite() {
		return quantite;
	}
	/**
	 * 
	 * @param quantite
	 */
	public void setQuantite(int quantite) {
		this.quantite = quantite;
	}
	/**
	 * 
	 */
	public String toString() {
		return this.getNom();
	}
}