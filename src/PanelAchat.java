import java.awt.Color;
import java.awt.Font;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Iterator;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;

@SuppressWarnings("serial")
/**
 * 
 * @author User-06
 *
 */
public class PanelAchat extends JPanel{
	/**
	 * 
	 * @param frame
	 */
	LocalDate date = LocalDate.now();
	PanelAchat(JFrame frame) {

		//Ajout de titre
		setBackground(new Color(0XF5F5DC));

		JLabel labelTypeAchatPA = new JLabel("Type d'achat: ");
		labelTypeAchatPA.setBounds(80, 30, 250, 30);
		labelTypeAchatPA.setFont(new Font("Times New Roman", Font.BOLD | Font.ITALIC, 22));
		add(labelTypeAchatPA);


		// Ajoute des textes et textField pour saisir les infos de client et médicament:  

		JLabel labelNomPA = new JLabel("Nom:");
		labelNomPA.setFont(new Font("Times New Roman", Font.PLAIN, 15));
		labelNomPA.setVisible(false);
		JLabel labelNumSecuPA = new JLabel("N° de la Sécurité Sociale:");
		labelNumSecuPA.setFont(new Font("Times New Roman", Font.PLAIN, 15));
		labelNumSecuPA.setVisible(false);
		JLabel labelSpecialistePA = new JLabel("Nom de Spécialiste:");
		labelSpecialistePA.setFont(new Font("Times New Roman", Font.PLAIN, 15));
		labelSpecialistePA.setVisible(false);
		JLabel labelMutuellePA = new JLabel("Mutuelle:");
		labelMutuellePA.setFont(new Font("Times New Roman", Font.PLAIN, 15));
		labelMutuellePA.setVisible(false);
		JLabel labelRemboursement = new JLabel("Remboursé par Mut.:");
		labelRemboursement.setFont(new Font("Times New Roman", Font.PLAIN, 15));
		labelRemboursement.setVisible(false);
		JLabel labelDateOrdonnancePA = new JLabel("Date d'ordonnance:");
		labelDateOrdonnancePA.setFont(new Font("Times New Roman", Font.PLAIN, 15));
		labelDateOrdonnancePA.setVisible(false);
		JLabel labelAjoutMedicamentPA = new JLabel("Médicament:");
		labelAjoutMedicamentPA.setFont(new Font("Times New Roman", Font.PLAIN, 15));
		labelAjoutMedicamentPA.setVisible(false);
		JLabel labelQuantiteMedicamentPA = new JLabel("Quantité:");
		labelQuantiteMedicamentPA.setFont(new Font("Times New Roman", Font.PLAIN, 15));
		labelQuantiteMedicamentPA.setVisible(false);

		// tailler et positionner les labels dans la page Achat

		labelNomPA.setBounds(100, 90, 250, 20);
		labelNumSecuPA.setBounds(100, 120, 250, 20);
		labelSpecialistePA.setBounds(550, 90, 250, 20);
		labelDateOrdonnancePA.setBounds(550, 60, 250, 20);
		labelAjoutMedicamentPA.setBounds(100, 250, 100, 20);
		labelQuantiteMedicamentPA.setBounds(100,280, 100, 20);
		labelMutuellePA.setBounds(550, 120, 250, 20);
		labelRemboursement.setBounds(550, 150, 250, 20);

		//Ajoute les labels dans le panle Achat

		add(labelNomPA);
		add(labelNumSecuPA);
		add(labelSpecialistePA);
		add(labelMutuellePA);
		add(labelDateOrdonnancePA);
		add(labelAjoutMedicamentPA);
		add(labelQuantiteMedicamentPA);
		add(labelRemboursement);

		// Tailler et positionner les textfields pour les labels de la page Achat
		//Ajoute des boutons

		JComboBox<String> comboBoxOrdonnance = new JComboBox<>();
		JComboBox<Client> cbNomPA = new JComboBox<>();
		JComboBox<Specialiste> cbSpecialistePA = new JComboBox<>();
		JComboBox<Medicament> cbMedicamentPA = new JComboBox<>();
		JComboBox<Integer> tfQuantiteMedicamentPA = new JComboBox<>();
		JTable table = new JTable();
		JScrollPane scrollPane = new JScrollPane(table);
		JTextField tfNumSecuPA= new JTextField();
		JTextField tfRemboursementPA = new JTextField();
		JTextField tfMutuellePA = new JTextField();
		JTextField tfDateOrdonnancePA = new JTextField();
		JButton buttonAjout = new JButton("Ajouter dans la liste d'achat");
		JButton buttonSupprimer = new JButton("Enlever de la liste d'achat");
		JButton buttonValiderAchat = new JButton("Valider l'achat");

		// Ajout de comboBox pour choisir si l'achat est avec ordonnance ou sans

		comboBoxOrdonnance.setBounds(270, 37, 200, 20);
		comboBoxOrdonnance.setFont(new Font("Times New Roman", Font.ITALIC, 14));
		comboBoxOrdonnance.setBackground(new Color(0xd9c0d5));
		comboBoxOrdonnance.addItem("Choisissez mode d'achat");
		comboBoxOrdonnance.addItem("Via Ordonnance");
		comboBoxOrdonnance.addItem("Sans Ordonnance");
		comboBoxOrdonnance.setFocusable(false);
		comboBoxOrdonnance.addActionListener(e ->{
			String choisirItem = (String) comboBoxOrdonnance.getSelectedItem();

			if (choisirItem.equals(comboBoxOrdonnance.getItemAt(0))) {

				labelNomPA.setVisible(false);
				labelNumSecuPA.setVisible(false);
				labelSpecialistePA.setVisible(false);
				labelMutuellePA.setVisible(false);
				labelDateOrdonnancePA.setVisible(false);
				labelAjoutMedicamentPA.setVisible(false);
				labelQuantiteMedicamentPA.setVisible(false);
				labelRemboursement.setVisible(false);
				tfRemboursementPA.setVisible(false);
				scrollPane.setVisible(false);

				cbNomPA.setVisible(false);
				tfNumSecuPA.setVisible(false);
				cbSpecialistePA.setVisible(false);
				tfDateOrdonnancePA.setVisible(false);
				tfQuantiteMedicamentPA.setVisible(false);
				tfMutuellePA.setVisible(false);

				cbMedicamentPA.setVisible(false);
				buttonValiderAchat.setVisible(false);
				buttonAjout.setVisible(false);
				buttonSupprimer.setVisible(false);
			}

			if (choisirItem.equals(comboBoxOrdonnance.getItemAt(1))){


				labelNomPA.setVisible(true);
				labelNumSecuPA.setVisible(true);
				labelSpecialistePA.setVisible(true);
				labelMutuellePA.setVisible(true);
				labelDateOrdonnancePA.setVisible(true);
				labelAjoutMedicamentPA.setVisible(true);
				labelQuantiteMedicamentPA.setVisible(true);
				labelRemboursement.setVisible(true);
				tfRemboursementPA.setVisible(true);
				scrollPane.setVisible(true);

				cbNomPA.setVisible(true);
				tfNumSecuPA.setVisible(true);
				cbSpecialistePA.setVisible(true);
				tfDateOrdonnancePA.setVisible(true);
				tfQuantiteMedicamentPA.setVisible(true);
				tfMutuellePA.setVisible(true);

				cbMedicamentPA.setVisible(true);

				buttonValiderAchat.setVisible(true);
				buttonAjout.setVisible(true);
				buttonSupprimer.setVisible(true);

			}

			if (choisirItem.equals(comboBoxOrdonnance.getItemAt(2))) {

				labelNomPA.setVisible(true);
				labelNumSecuPA.setVisible(true);
				labelMutuellePA.setVisible(false);
				labelSpecialistePA.setVisible(false);
				labelDateOrdonnancePA.setVisible(false);
				labelAjoutMedicamentPA.setVisible(true);
				labelQuantiteMedicamentPA.setVisible(true);
				labelRemboursement.setVisible(false);
				tfRemboursementPA.setVisible(false);
				scrollPane.setVisible(true);
				cbNomPA.setVisible(true);
				tfNumSecuPA.setVisible(true);
				cbSpecialistePA.setVisible(false);
				tfDateOrdonnancePA.setVisible(false);
				tfQuantiteMedicamentPA.setVisible(true);
				buttonValiderAchat.setVisible(true);
				cbMedicamentPA.setVisible(true);
				buttonSupprimer.setVisible(true);
				tfMutuellePA.setVisible(false);
				buttonAjout.setVisible(true);
			}
		});

		//NOM: Pour trouver le nom de client

		cbNomPA.setBounds(270, 90, 200, 20);
		cbNomPA.setFont(new Font("Times New Roman", Font.ITALIC, 14));
		cbNomPA.setBackground(new Color(0xd9c0d5));
		cbNomPA.setEditable(true);
		cbNomPA.addItem(null);

		// Ajoute des noms client dans le comboBox
		for (Client client : LesListes.LesClients) {
			cbNomPA.addItem(client);
		};

		// Action pour le combo Box nom pour auto remplir les autres TextField

		cbNomPA.addActionListener(e ->{

			try {
				for (Client client : LesListes.LesClients) {
					if ((((Client) cbNomPA.getSelectedItem()).getNom().toUpperCase().equals(client.getNom()) && ((Client) cbNomPA.getSelectedItem()).getPrenom().equals(client.getPrenom()))) {
						tfNumSecuPA.setText(((Client) cbNomPA.getSelectedItem()).getNumSecuriteSociale());
						tfMutuellePA.setText(client.getMutuelle());
					}
					for (Mutuelle mutuelle: LesListes.lesMutuelles) {
						if (tfMutuellePA.getText().equals(mutuelle.getNom())) {
							tfRemboursementPA.setText(String.valueOf( mutuelle.getRemboursement()));
						}
					}
				}
			}
			catch (NullPointerException NPE) {}
		});

		// SECURITE SOCIALE:

		tfNumSecuPA.setBounds(270, 120, 200, 20);
		tfNumSecuPA.setFont(new Font("Times New Roman", Font.ITALIC, 14));
		tfNumSecuPA.setEditable(false);

		//Mutuelle:

		tfMutuellePA.setBounds(680, 120, 200, 20);
		tfMutuellePA.setFont(new Font("Times New Roman", Font.ITALIC, 14));
		tfMutuellePA.setEditable(false);

		//SPECIALISTE:

		cbSpecialistePA.setBounds(680, 90, 200, 20);
		cbSpecialistePA.setFont(new Font("Times New Roman", Font.ITALIC, 14));
		cbSpecialistePA.setBackground(new Color(0xd9c0d5));
		cbSpecialistePA.setEditable(true);
		cbSpecialistePA.addItem(null);

		// Ajoute des specialistes dans le comboBox specialiste

		for (Specialiste specialiste : LesListes.lesSpecialistes) {
			cbSpecialistePA.addItem(specialiste);
		};

		// Ajoute d'un comboBox pour les médicaments

		cbMedicamentPA.setBounds(180, 250, 180, 20);
		cbMedicamentPA.setFont(new Font("Times New Roman", Font.ITALIC, 14));
		cbMedicamentPA.setBackground(new Color(0xd9c0d5));
		cbMedicamentPA.setEditable(true);
		cbMedicamentPA.addItem(null);

		// Ajoute des medicaments dans le comboBox medicament

		for (Medicament medicament: LesListes.lesMedicaments) {
			cbMedicamentPA.addItem(medicament);
		};		

		//textField pour la quantité des médicaments

		tfQuantiteMedicamentPA.setBounds(180, 280, 180, 20);
		tfQuantiteMedicamentPA.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		tfQuantiteMedicamentPA.setBackground(new Color(0xd9c0d5));
		tfQuantiteMedicamentPA.setEditable(true);
		tfQuantiteMedicamentPA.addItem(1);
		tfQuantiteMedicamentPA.addItem(2);
		tfQuantiteMedicamentPA.addItem(3);
		tfQuantiteMedicamentPA.addItem(4);
		tfQuantiteMedicamentPA.addItem(5);
		tfQuantiteMedicamentPA.addItem(6);
		tfQuantiteMedicamentPA.addItem(7);
		tfQuantiteMedicamentPA.addItem(8);
		tfQuantiteMedicamentPA.addItem(9);
		tfQuantiteMedicamentPA.addItem(10);

		//TextField pour la date d'ordonnance

		tfDateOrdonnancePA.setBounds(680, 60, 200, 20);
		tfDateOrdonnancePA.setForeground(Color.DARK_GRAY);
		tfDateOrdonnancePA.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		tfDateOrdonnancePA.setEditable(false);
		String formattedDate = date.format(DateTimeFormatter.ofPattern("dd-MMM-yyyy"));
		tfDateOrdonnancePA.setText(formattedDate);

		//Label pour le montant remboursé

		tfRemboursementPA.setBounds(680, 150, 200, 20);
		tfRemboursementPA.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		tfRemboursementPA.setEditable(false);

		cbNomPA.setVisible(false);
		tfNumSecuPA.setVisible(false);
		cbSpecialistePA.setVisible(false);
		tfMutuellePA.setVisible(false);
		tfRemboursementPA.setVisible(false);
		tfDateOrdonnancePA.setVisible(false);
		labelQuantiteMedicamentPA.setVisible(false);
		tfQuantiteMedicamentPA.setVisible(false);
		scrollPane.setVisible(false);
		cbMedicamentPA.setVisible(false);

		add(cbNomPA);
		add(tfNumSecuPA);
		add(cbSpecialistePA);
		add(tfDateOrdonnancePA);
		add(tfMutuellePA);
		add(cbMedicamentPA);
		add(labelQuantiteMedicamentPA);
		add(tfQuantiteMedicamentPA);
		add(tfRemboursementPA);


		// Ajoute de table

		String header [] = {"Nom", "Catégorie", "Prix", "Qnt.", "Mutuelle", "(%)Remb.", "Total"};

		DefaultTableModel model= new DefaultTableModel(header, 0);
		table.setModel(model);
		Object rowData[] = new Object[7];

		table.setSurrendersFocusOnKeystroke(true);
		table.setFont(new Font("Times New Roman", Font.PLAIN, 13));
		table.setCellSelectionEnabled(true);
		table.setRowSelectionAllowed(true);
		table.setEnabled(true);

		table.getColumnModel().getColumn(0).setResizable(false);
		table.getColumnModel().getColumn(0).setPreferredWidth(60);
		table.getColumnModel().getColumn(0).setMinWidth(40);
		table.getColumnModel().getColumn(1).setPreferredWidth(90);
		table.getColumnModel().getColumn(1).setMinWidth(60);
		table.getColumnModel().getColumn(2).setPreferredWidth(20);
		table.getColumnModel().getColumn(2).setMinWidth(20);
		table.getColumnModel().getColumn(3).setPreferredWidth(20);
		table.getColumnModel().getColumn(3).setMinWidth(20);
		table.getColumnModel().getColumn(4).setPreferredWidth(60);
		table.getColumnModel().getColumn(4).setMinWidth(40);
		table.getColumnModel().getColumn(5).setPreferredWidth(40);
		table.getColumnModel().getColumn(5).setMinWidth(20);
		table.getColumnModel().getColumn(6).setPreferredWidth(60);
		table.getColumnModel().getColumn(6).setMinWidth(40);

		table.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));

		JTableHeader header1 = table.getTableHeader();
		header1.setBackground(new Color(0xFFD700));

		// Ajout de Scroll pour la liste dans la fenêtre de la liste des clients

		scrollPane.setBounds(380, 250, 550, 250);
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setToolTipText("");
		scrollPane.setViewportBorder(new BevelBorder(BevelBorder.RAISED, Color.LIGHT_GRAY, null, null, null));
		add(scrollPane);

		// Donner l'action au bouton Ajout pour Ajouter les medicament dans la liste pour l'achat

		ArrayList<String> listMedicamentEnregistrer = new ArrayList<>();

		buttonAjout.setFont(new Font("Times New Roman", Font.PLAIN, 13));
		buttonAjout.setBounds(150, 330, 170, 30);
		buttonAjout.setFocusable(false);
		buttonAjout.setVisible(false);
		buttonAjout.setBackground(new Color(0xFFD700));
		buttonAjout.addActionListener(e ->{

			for(Client client: LesListes.LesClients) {
				String nom = ((Client) cbNomPA.getSelectedItem()).getNom();
				String prenom = ((Client) cbNomPA.getSelectedItem()).getPrenom();
				String medicamentTampon = ((Medicament) cbMedicamentPA.getSelectedItem()).getNom();
				if ((nom.toUpperCase().equals(client.getNom()) && prenom.equals(client.getPrenom())) && tfNumSecuPA.getText().equals(client.getNumSecuriteSociale())) {
					for (Medicament medicament: LesListes.lesMedicaments) {
						if(medicamentTampon.equals(medicament.getNom())) {

							int prix = medicament.getPrix();
							int quantite = (int) tfQuantiteMedicamentPA.getSelectedItem();
							int rembourse = Integer.parseInt(tfRemboursementPA.getText());

							rowData[0] = medicament.getNom();
							rowData[1] = medicament.getCategorie();
							rowData[2] = medicament.getPrix();
							rowData[3] = tfQuantiteMedicamentPA.getSelectedItem();
							rowData[4] = tfMutuellePA.getText();
							for (Mutuelle mutuelle: LesListes.lesMutuelles) {
								int remboursement = (int) mutuelle.getRemboursement();
								if (tfMutuellePA.getText().equals(mutuelle.getNom())) {
									rowData[5] = remboursement;
								}
							}
							rowData[6] = (prix * quantite) - (prix * quantite) * rembourse / 100;
							model.addRow(rowData);
	
							listMedicamentEnregistrer.add(((Medicament) cbMedicamentPA.getSelectedItem()).getNom());
						}
					}
				}
			}
		});

		// Donner l'action au bouton Affiche pour Afficher la liste d'achat

		buttonSupprimer.setFont(new Font("Times New Roman", Font.PLAIN, 13));
		buttonSupprimer.setBounds(150, 380, 170, 30);
		buttonSupprimer.setFocusable(false);
		buttonSupprimer.setVisible(false);
		buttonSupprimer.setBackground(new Color(0xFFD700));
		buttonSupprimer.addActionListener(e ->{
			
			listMedicamentEnregistrer.remove(table.getSelectedRow());
			model.removeRow(table.getSelectedRow());

		});

		// Donner l'action au bouton ValierAchat pour Valider les achats saisis

		buttonValiderAchat.setFont(new Font("Times New Roman", Font.PLAIN, 13));
		buttonValiderAchat.setBounds(150, 430, 170, 30);
		buttonValiderAchat.setFocusable(false);
		buttonValiderAchat.setVisible(false);
		buttonValiderAchat.setBackground(new Color(0xFFD700));
		buttonValiderAchat.addActionListener(e ->{

			for (Client client : LesListes.LesClients) {
				String nom = ((Client) cbNomPA.getSelectedItem()).getNom();
				String specialiste = ((Specialiste) cbSpecialistePA.getSelectedItem()).getNom();

				if (nom.equals(client.getNom())) {
					Ordonnance ordonnance = new Ordonnance(date, client.getMedecinTraitant(), client.getNom(), listMedicamentEnregistrer, specialiste);
					LesListes.addOrdonnances(ordonnance);
				}

			}
		});

		// Donner l'action au bouton AchatJour pour consulter la liste d'achat de la journée


		JButton buttonAchatJour = new JButton("Achat de jour");
		buttonAchatJour.setBounds(0, 530, 800, 50);
		buttonAchatJour.setFont(new Font("Times New Roman", Font.ITALIC| Font.BOLD, 18));
		buttonAchatJour.setFocusable(false);
		buttonAchatJour.setBorder(null);
		buttonAchatJour.setBackground(new Color(0xd9c0d5));
		buttonAchatJour.addActionListener(e ->{
			
				
		});

		// Donner l'action au bouton Quitter

		JButton buttonSortirAchat = new JButton("Quitter");
		buttonSortirAchat.setFont(new Font("Garamond", Font.BOLD | Font.ITALIC, 18));
		buttonSortirAchat.setBorder(null);
		buttonSortirAchat.setBounds(800, 530, 150, 30);
		buttonSortirAchat.setFocusable(false);
		buttonSortirAchat.setBackground(null);
		buttonSortirAchat.addActionListener(e ->{
			System.exit(0);
		});



		add(comboBoxOrdonnance);
		add(buttonAjout);
		add(buttonSupprimer);
		add(buttonValiderAchat);
		add(buttonAchatJour);
		add(buttonSortirAchat);

		setLayout(null);
	}
}
