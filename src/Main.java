import java.awt.Color;
import java.awt.Font;
import java.time.LocalDate;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTabbedPane;
import exceptions.MissMatchException;
/**
 * 
 * @author ehsan bayat
 *
 */
@SuppressWarnings("serial")
public class Main extends JFrame{

	JTabbedPane tabbedPane = new JTabbedPane();
	PanelAchat panelAchat = new PanelAchat(this);
	PanelAcceuil panelAcceuil = new PanelAcceuil(this);
	PanelClient panelClient = new PanelClient(this);
	PanelOrdonnance panelOrdonnance = new PanelOrdonnance(this);

	ImageIcon img = new ImageIcon("iconPharmacie.png");

	/**
	 * 
	 */
	Main(){

		this.setTitle("Sparadrap");
		this.setIconImage(img.getImage());
		getContentPane().setLayout(null);
		this.getContentPane().setBackground(new Color(0xF4F7FC));

		//Ajout de titre (Sparadrap) en tête de page

		JLabel Sparadrap = new JLabel("Sparadrap");
		Sparadrap.setBounds(350, 30, 300 ,60);
		Sparadrap.setFont(new Font("Georgia", Font.BOLD, 52));
		Sparadrap.setForeground(Color.MAGENTA);

		// Ajouter tabbedPane (panels dans le frame)

		tabbedPane.setBounds(0, 100, 985, 610);
		tabbedPane.setBackground(new Color(0XF0F0F0));

		tabbedPane.add("Page d'acceuil", panelAcceuil);
		tabbedPane.add("Client", panelClient);
		tabbedPane.add("Achat", panelAchat);
		tabbedPane.add("Ordonnance", panelOrdonnance);

		JLabel iconLabel = new JLabel();
		iconLabel.setIcon(img);
		iconLabel.setBounds(850, 0, 120, 120);

		this.setSize(1000, 750);
		this.setResizable(false);
		getContentPane().add(Sparadrap);
		getContentPane().add(iconLabel);
		getContentPane().add(tabbedPane);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.setVisible(true);

	}

	/**
	 * 
	 * @param args
	 * @throws MissMatchException 
	 */
	public static void main(String[] args) throws MissMatchException {
		/**
		 * 
		 */

		// Ajoute des clients

		Dates dateTomporaire1 = new Dates (1960, 8, 19);
		Dates dateTomporaire2 = new Dates (27, 1, 1993);
		LesListes.addClient(new Client ("Bayat", "Ehsan", dateTomporaire1, "Rue de la Seille", 11, 8721, 54320, "Maxéville", 780248467, "m.ehsanbayat@gmail.com", "122333444455555", "Miltis", "DR. Philipe"));
		LesListes.addClient(new Client ("Koothrappali", "Terence", dateTomporaire2, "Rue de la ...", 13, 3, 54000, "Lunéville", 780248467, "koothrappali@gmail.fr", "555554444333221", "Mutami", "DR. Dupont"));

		//Ajoute des specialistes

		LesListes.addSpecialistes(new Specialiste("perrin", "Sylvie", "Rue Vauban", 6, 0, 54000, "Nancy", 383901601, "sylvie@gmail.fr", "Dermatoloque"));
		LesListes.addSpecialistes(new Specialiste("Bassnagel", "Philippe", "Avenue Raymond Pinchard", 1240, 0, 54100, "Nanacy", 383944346, "philippe.gmail.fr", "Radiologue"));
		LesListes.addSpecialistes(new Specialiste("saintot", "Manon", "Rue André Bisiaux", 132, 0, 54320, "Maxéville", 383676166,"manon.gmail.fr", "Allergologue"));
		LesListes.addSpecialistes(new Specialiste("Hay", "Arélien", "Rue des Carmes", 44, 0, 54000, "Nancy", 383191900, "aurelien.gmail.fr", "Ophtalmologue"));
		LesListes.addSpecialistes(new Specialiste ("Amira", "Rouabah", "Rue Ambroise Paré", 95, 0, 54000, "Nancy", 383944008, "amira.gmail.fr", "Pédiatre"));

		//Ajoute des medecins traitants

		LesListes.addMedecinTraitant(new Medecin ("Meziane", "Lilas", "TER Pl. Carnot", 1, 0, 54000, "Nancy", 383940327, "lilas.gmail.fr", 43435434));
		LesListes.addMedecinTraitant(new Medecin ("Canton", "Valérie", "Av. Anatole", 34, 0, 54000, "Nancy", 383281757, "valerie.gmail.fr", 45345643));
		LesListes.addMedecinTraitant(new Medecin ("Steff", "Christine", "Bd de la Mothe", 1, 0, 54000, "Nancy", 383293647, "christine.gmail.fr", 23987065));


		Dates dateGentamicine = new Dates (23, 3, 2023);
		Dates dateLoperamide = new Dates (12, 5, 2024);
		Dates dateCetrizine = new Dates (2, 10, 2023);
		Dates dateMagnesium = new Dates (5, 12, 2022);
		Dates dateAspirine = new Dates (13, 8, 2024);
		Dates dateAntiacneiques = new Dates (28, 9, 2023);
		Dates dateAnlerine = new Dates (20, 1, 2025);
		Dates dateNifedipine = new Dates (20, 1, 2025);

		//Ajoute des médicaments

		LesListes.addMedicaments(new Medicament("Gentamicine", "Antibiotique", 10, dateGentamicine, 200));
		LesListes.addMedicaments(new Medicament("Lopéramide", "Antidiarrhéiques", 15, dateLoperamide, 100));
		LesListes.addMedicaments(new Medicament("Cétrizine", "Antihistaminiques", 8, dateCetrizine, 300));
		LesListes.addMedicaments(new Medicament("Hydroxyde de magnésium", "Antiacides", 12, dateMagnesium, 130));
		LesListes.addMedicaments(new Medicament("Aspirine", "Antiagrégant", 5, dateAspirine, 400));
		LesListes.addMedicaments(new Medicament("Antiacnéïques", "Dermatologie", 17, dateAntiacneiques, 100));
		LesListes.addMedicaments(new Medicament("Alvérine", "Antispasmodiques", 14, dateAnlerine, 80));
		LesListes.addMedicaments(new Medicament("Nifédipine", "Antagonistes du calcium", 13, dateNifedipine, 320));

		//Ajoute des mutuelles santé

		LesListes.addMutuelles(new Mutuelle("Miltis", "Cr Albert Thomas", 33, 69003, "Lyon", 427852788, "mutuelle@miltis.fr", "Rhône", 40));
		LesListes.addMutuelles(new Mutuelle("harmonie", "Rue Saint-Jean", 19, 54000, "Nancy", 980980880, "accessibilite-numerique@harmonie-mutuelle.fr", "Meurthe & Moselle", 55));
		LesListes.addMutuelles(new Mutuelle("MIF", "Rue Yves Toudic", 23, 75481, "Paris", 970157777, "mutuelle@MIF.fr", "Paris", 60));
		LesListes.addMutuelles(new Mutuelle("Mutami", "Cr Gambetta", 16, 34000, "Montpelier", 467045350, "mutuelle@mutami.fr", "Hérault", 50));
		LesListes.addMutuelles(new Mutuelle("Mut'Est", "Bd du Président-Wilson", 11, 67000, "Strasbourg", 969363232, "mutuelle@mutest.fr", "Bas-Rhin", 60));

		//Ajoute des ordonnances

		LocalDate dateOrdonnance1 = LocalDate.of (2022, 4, 23);
		LocalDate dateOrdonnance2 = LocalDate.of(2022, 5, 5);
		
		new Main();

//		LesListes.addOrdonnances(new Ordonnance(dateOrdonnance1, "Meziane", "Steven", "Cétrizine, Antiacnéïques" , "perrin"));
//		LesListes.addOrdonnances(new Ordonnance(dateOrdonnance2, "Canton", "Robert", "Nifédipine, Hydroxyde de magnésium",  null));

		//	LesListes.addListAchats(new List("dfdqf", "fdqfdq", "kjlmjlk", "jklmjfkd", "fdklmqjf", "fdlqjff", "fjlkdmq"));
	}


}


