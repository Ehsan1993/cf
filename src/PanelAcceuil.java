import java.awt.Color;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
/**
 * 
 * @author User-06
 *
 */
public class PanelAcceuil extends JPanel {
	
	ImageIcon img = new ImageIcon("iconPharmacie.png");
	JButton buttonInfoClientPD, buttonListeClientPD, buttonListeAchatPD, buttonAchatJourPD, buttonListeOrdonnancePD, buttonQuitterPD;
	/**
	 * 
	 * @param frame
	 */
	PanelAcceuil(JFrame frame){
		
		setBackground(new Color(0XF5F5DC));
		
		// Ajoute de label background

		JLabel labelListeOrdonnancePD = new JLabel("Sparadrap");
		labelListeOrdonnancePD.setFont(new Font("Times New Roman", Font.ITALIC | Font.BOLD, 120));
		labelListeOrdonnancePD.setBounds(300, 0, 700, 500);
		labelListeOrdonnancePD.setForeground(new Color(0xd9c0d5));
		add(labelListeOrdonnancePD);

		// Insérer les boutons dans la page d'acceuil

		ImageIcon buttonAcceuil = new ImageIcon("buttonAcceuil.png");
		buttonInfoClientPD = new JButton();
		buttonListeClientPD = new JButton();
		buttonListeAchatPD = new JButton();
		buttonAchatJourPD = new JButton();
		buttonListeOrdonnancePD = new JButton();
		buttonQuitterPD = new JButton("Quitter");


		// Donner l'action pour le bouton Clique qui nous amène à la fenetre de détail d'un client

		buttonInfoClientPD.setIcon(new ImageIcon("buttonAcceuil.png"));
		buttonInfoClientPD.setBounds(0, 50, 43, 37);
		buttonInfoClientPD.setBorder(null);
		buttonInfoClientPD.setFocusable(false);
		buttonInfoClientPD.addMouseListener(new MouseAdapter() {
			public void mouseEntered(MouseEvent e) {

				buttonInfoClientPD.setBounds(0, 50, 270, 40);
				buttonInfoClientPD.setBackground(new Color(0xFFD700));
				buttonInfoClientPD.setIcon(null);
				buttonInfoClientPD.setText("Trouver l'information d'un client");
				buttonInfoClientPD.setFont(new Font("Times new Roman", Font.BOLD| Font.ITALIC, 16));
			}

			public void mouseExited(MouseEvent e) {
				buttonInfoClientPD.setBounds(0, 50, 43, 37);
				buttonInfoClientPD.setText(null);
				buttonInfoClientPD.setIcon(new ImageIcon("buttonAcceuil.png"));
			}

			public void mouseClicked(MouseEvent e) {
				
				new FenetreInfoClient();
				frame.dispose();
				
			}
		});


		//Action pour le bouton Clique qui nous amène à la liste des clients

		buttonListeClientPD.setIcon(new ImageIcon("buttonAcceuil.png"));
		buttonListeClientPD.setBounds(0, 100, 43, 37);
		buttonListeClientPD.setBorder(null);
		buttonListeClientPD.setFocusable(false);
		buttonListeClientPD.addMouseListener(new MouseAdapter() {
			public void mouseEntered(MouseEvent e) {

				buttonListeClientPD.setBounds(0, 100, 270, 40);
				buttonListeClientPD.setBackground(new Color(0xFFD700));
				buttonListeClientPD.setIcon(null);
				buttonListeClientPD.setText("Accèder à la liste des clients");
				buttonListeClientPD.setFont(new Font("Times new Roman", Font.BOLD| Font.ITALIC, 16));
			}

			public void mouseExited(MouseEvent e) {
				buttonListeClientPD.setBounds(0, 100, 43, 37);
				buttonListeClientPD.setText(null);
				buttonListeClientPD.setIcon(new ImageIcon("buttonAcceuil.png"));
			}

			public void mouseClicked(MouseEvent e) {

				new FenetreListeClients();
				frame.dispose();
			}
		});

		//Action pour le bouton Clique qui nous amène à la liste des achats

		buttonListeAchatPD.setIcon(new ImageIcon("buttonAcceuil.png"));
		buttonListeAchatPD.setBounds(0, 150, 43, 37);
		buttonListeAchatPD.setBorder(null);
		buttonListeAchatPD.setFocusable(false);
		buttonListeAchatPD.addMouseListener(new MouseAdapter() {
			public void mouseEntered(MouseEvent e) {

				buttonListeAchatPD.setBounds(0, 150, 270, 40);
				buttonListeAchatPD.setBackground(new Color(0xFFD700));
				buttonListeAchatPD.setIcon(null);
				buttonListeAchatPD.setText("Accèder à la liste des achats");
				buttonListeAchatPD.setFont(new Font("Times new Roman", Font.BOLD| Font.ITALIC, 16));
			}

			public void mouseExited(MouseEvent e) {

				buttonListeAchatPD.setBounds(0, 150, 43, 37);
				buttonListeAchatPD.setText(null);
				buttonListeAchatPD.setIcon(new ImageIcon("buttonAcceuil.png"));
			}

			public void mouseClicked(MouseEvent e) {
				new FenetreListeAchat();
				frame.dispose();

			}
		});

		//Action pour le bouton Clique qui nous amène à la liste des clients de la journée

		buttonAchatJourPD.setIcon(new ImageIcon("buttonAcceuil.png"));
		buttonAchatJourPD.setBounds(0, 200, 43, 37);
		buttonAchatJourPD.setBorder(null);
		buttonAchatJourPD.setFocusable(false);
		buttonAchatJourPD.addMouseListener(new MouseAdapter() {
			public void mouseEntered(MouseEvent e) {

				buttonAchatJourPD.setBounds(0, 200, 270, 40);
				buttonAchatJourPD.setBackground(new Color(0xFFD700));
				buttonAchatJourPD.setIcon(null);
				buttonAchatJourPD.setText("Accèder aux achats du jour");
				buttonAchatJourPD.setFont(new Font("Times new Roman", Font.BOLD| Font.ITALIC, 16));
			}

			public void mouseExited(MouseEvent e) {

				buttonAchatJourPD.setBounds(0, 200, 43, 37);
				buttonAchatJourPD.setText(null);
				buttonAchatJourPD.setIcon(new ImageIcon("buttonAcceuil.png"));
			}

			public void mouseClicked(MouseEvent e) {

			}
		});


		//Action pour le bouton Clique qui nous amène à la liste des ordonnances

		buttonListeOrdonnancePD.setIcon(new ImageIcon("buttonAcceuil.png"));
		buttonListeOrdonnancePD.setBounds(0, 250, 43, 37);
		buttonListeOrdonnancePD.setBorder(null);
		buttonListeOrdonnancePD.setFocusable(false);
		buttonListeOrdonnancePD.addMouseListener(new MouseAdapter() {
			public void mouseEntered(MouseEvent e) {

				buttonListeOrdonnancePD.setBounds(0, 250, 270, 40);
				buttonListeOrdonnancePD.setBackground(new Color(0xFFD700));
				buttonListeOrdonnancePD.setIcon(null);
				buttonListeOrdonnancePD.setText("Accèder à la liste des ordonnances");
				buttonListeOrdonnancePD.setFont(new Font("Times new Roman", Font.BOLD| Font.ITALIC, 16));
			}

			public void mouseExited(MouseEvent e) {

				buttonListeOrdonnancePD.setBounds(0, 250, 43, 37);
				buttonListeOrdonnancePD.setText(null);
				buttonListeOrdonnancePD.setIcon(new ImageIcon("buttonAcceuil.png"));
			}

			public void mouseClicked(MouseEvent e) {

				new FenetreListeOrdonnance();
				frame.dispose();

			}
		});

		// le bouton de sortir de la page d'acceuil

		buttonQuitterPD.setBounds(800, 530, 150, 30);
		buttonQuitterPD.setFont(new Font("Garamond", Font.BOLD | Font.ITALIC, 18));
		buttonQuitterPD.setBorder(null);
		buttonQuitterPD.setBackground(null);
		buttonQuitterPD.setFocusable(false);
		buttonQuitterPD.addActionListener(e ->{
			System.exit(0);
		});

		add(buttonInfoClientPD);
		add(buttonListeClientPD);
		add(buttonListeAchatPD);
		add(buttonAchatJourPD);
		add(buttonListeOrdonnancePD);
		add(buttonQuitterPD);
		setLayout(null);

	}
}
