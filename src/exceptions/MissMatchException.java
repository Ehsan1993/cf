package exceptions;
import javax.swing.JOptionPane;

public class MissMatchException extends Exception {

	public MissMatchException() {
		// TODO Auto-generated constructor stub
	}

	public MissMatchException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
		JOptionPane.showInternalMessageDialog(null, message, "Erreur frappe", JOptionPane.ERROR_MESSAGE);
	}

	public MissMatchException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public MissMatchException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public MissMatchException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
