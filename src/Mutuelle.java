/**
 * 
 * @author User-06
 *
 */
public class Mutuelle {

	String nom;
	String nomRue;
	int numRue;
	long codePostal;
	String ville;
	long phone;
	String mail;
	String departement;
	int remboursement;
	/**
	 * 
	 * @param nom
	 * @param nomRue
	 * @param numRue
	 * @param codePostal
	 * @param ville
	 * @param phone
	 * @param mail
	 * @param departement
	 * @param remboursement
	 */
	Mutuelle(String nom, String nomRue, int numRue, long codePostal, String ville, long phone, String mail, String departement, int remboursement){

		this.nom = nom;
		this.numRue = numRue;
		this.nomRue = nomRue;
		this.codePostal = codePostal;
		this.ville = ville;
		this.phone = phone;
		this.mail = mail;
		this.departement = departement;
		this.remboursement = remboursement;
	}
	/**
	 * 
	 * @return
	 */
	public String getNom() {
		return nom;
	}
	/**
	 * 
	 * @param nom
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}
	/**
	 * 
	 * @return
	 */
	public String getNomRue() {
		return nomRue;
	}
	/**
	 * 
	 * @param nomRue
	 */
	public void setNomRue(String nomRue) {
		this.nomRue = nomRue;
	}
	/**
	 * 
	 * @return
	 */
	public int getNumRue() {
		return numRue;
	}
	/**
	 * 
	 * @param numRue
	 */
	public void setNumRue(int numRue) {
		this.numRue = numRue;
	}
	/**
	 * 
	 * @return
	 */
	public long getCodePostal() {
		return codePostal;
	}
	/**
	 * 
	 * @param codePostal
	 */
	public void setCodePostal(long codePostal) {
		this.codePostal = codePostal;
	}
	/**
	 * 
	 * @return
	 */
	public String getVille() {
		return ville;
	}
	/**
	 * 
	 * @param ville
	 */
	public void setVille(String ville) {
		this.ville = ville;
	}
	/**
	 * 
	 * @return
	 */
	public long getPhone() {
		return phone;
	}
	/**
	 * 
	 * @param phone
	 */
	public void setPhone(long phone) {
		this.phone = phone;
	}
	/**
	 * 
	 * @return
	 */
	public String getMail() {
		return mail;
	}
	/**
	 * 
	 * @param mail
	 */
	public void setMail(String mail) {
		this.mail = mail;
	}
	/**
	 * 
	 * @return
	 */
	public String getDepartement() {
		return departement;
	}
	/**
	 * 
	 * @param departement
	 */
	public void setDepartement(String departement) {
		this.departement = departement;
	}
	/**
	 * 
	 * @return
	 */
	public int getRemboursement() {
		return remboursement;
	}
	/**
	 * 
	 * @param remboursement
	 */
	public void setRemboursement(int remboursement) {
		this.remboursement = remboursement;
	}
	/**
	 * 
	 */
	public String toString() {
		return "Nom: " + this.getNom() + " Adresse: " + this.getNumRue() + " " + this.getNomRue() + ", " + this.getVille() + ", " + this.getCodePostal() + " Département: " + this.getDepartement() + " Mail: " + this.getMail() + " Phone: " + this.getPhone() + " Remboursement: " + this.getRemboursement() + " %";
	}
}


