/**
 * 
 * @author User-06
 *
 */
public class Dates {

	long annee;
	int mois;
	int jour;
	/**
	 * 
	 * @param jour
	 * @param mois
	 * @param annee
	 */
	Dates(int jour, int mois, long annee){

		this.jour = jour;
		this.mois = mois;
		this.annee = annee;
	}
	/**
	 * 
	 * @return
	 */
	public long getAnnee() {
		return annee;
	}
	/**
	 * 
	 * @param annee
	 */
	public void setAnnee(long annee) {
		this.annee = annee;
	}
	/**
	 * 
	 * @return
	 */
	public int getMois() {
		return mois;
	}
	/**
	 * 
	 * @param mois
	 */
	public void setMois(int mois) {
		this.mois = mois;
	}
	/**
	 * 
	 * @return
	 */
	public int getJour() {
		return jour;
	}
	/**
	 * 
	 * @param jour
	 */
	public void setJour(int jour) {
		this.jour = jour;
	}
	/**
	 * 
	 */
	public String toString () {
		return this.getJour() + "/" + this.getMois() + "/" + this.getAnnee();
	}
}


