import java.awt.Color;
import java.awt.Font;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.text.NumberFormat;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.text.NumberFormatter;

import exceptions.MissMatchException;
/**
 * 
 * @author User-06
 *
 */
public class PanelClient extends JPanel {
/**
 * 
 * @param frame
 */
	PanelClient(JFrame frame){

		setBackground(new Color(0XF5F5DC));
		
		// création des labels pour les textes

		JLabel labelNouveauClientPC = new JLabel("Nouveau client");
		labelNouveauClientPC.setFont(new Font("Times New Roman", Font.BOLD | Font.ITALIC, 22));
		JLabel labelNomPC = new JLabel("Nom:");
		labelNomPC.setFont(new Font("Times New Roman", Font.PLAIN, 15));
		JLabel labelPrenomPC = new JLabel("Prénom:");
		labelPrenomPC.setFont(new Font("Times New Roman", Font.PLAIN, 15));
		JLabel labelDateNaissancePC = new JLabel("Date de naissance:");
		labelDateNaissancePC.setFont(new Font("Times New Roman", Font.PLAIN, 15));
		JLabel labelJourPC = new JLabel("Jour:");
		labelJourPC.setFont(new Font("Times New Roman", Font.PLAIN, 15));
		JLabel labelMoisPC = new JLabel("Mois:");
		labelMoisPC.setFont(new Font("Times New Roman", Font.PLAIN, 15));
		JLabel labelAnneePC = new JLabel("Année:");
		labelAnneePC.setFont(new Font("Times New Roman", Font.PLAIN, 15));
		JLabel labelAdressePC = new JLabel("Adresse:");
		labelAdressePC.setFont(new Font("Times New Roman", Font.PLAIN, 15));
		JLabel labelNumRuePC = new JLabel("N° de la rue:");
		labelNumRuePC.setFont(new Font("Times New Roman", Font.PLAIN, 15));
		JLabel labelNomRuePC = new JLabel("Nom de la rue:");
		labelNomRuePC.setFont(new Font("Times New Roman", Font.PLAIN, 15));
		JLabel labelNumLogementPC = new JLabel("N° de logement:");
		labelNumLogementPC.setFont(new Font("Times New Roman", Font.PLAIN, 15));
		JLabel labelCodePostalPC = new JLabel("Code Postal:");
		labelCodePostalPC.setFont(new Font("Times New Roman", Font.PLAIN, 15));
		JLabel labelVillePC = new JLabel("Ville:");
		labelVillePC.setFont(new Font("Times New Roman", Font.PLAIN, 15));
		JLabel labelPhonePC = new JLabel("N° de téléphone:");
		labelPhonePC.setFont(new Font("Times New Roman", Font.PLAIN, 15));
		JLabel labelEmailPC = new JLabel("Adresse mail:");
		labelEmailPC.setFont(new Font("Times New Roman", Font.PLAIN, 15));
		JLabel labelNumSecuPC = new JLabel("N° de Sécurité Sociale:");
		labelNumSecuPC.setFont(new Font("Times New Roman", Font.PLAIN, 15));
		JLabel labelMutuellePC = new JLabel("Mutuelle:");
		labelMutuellePC.setFont(new Font("Times New Roman", Font.PLAIN, 15));
		JLabel labelMedecinPC = new JLabel("Médecin Traitant:");
		labelMedecinPC.setFont(new Font("Times New Roman", Font.PLAIN, 15));
		JLabel labelDejaClientPC = new JLabel("Déjà client");
		labelDejaClientPC.setFont(new Font("Times New Roman", Font.BOLD | Font.ITALIC, 22));
		JLabel labelNomDejaClientPC = new JLabel("Nom:");
		labelNomDejaClientPC.setFont(new Font("Times New Roman", Font.PLAIN, 15));
		JLabel labelPrenomDejaClientPC = new JLabel("Prénom:");
		labelPrenomDejaClientPC.setFont(new Font("Times New Roman", Font.PLAIN, 15));
		JLabel labelNumSecuDejaClientPC = new JLabel("N° de la Sécurité Sociale:");
		labelNumSecuDejaClientPC.setFont(new Font("Times New Roman", Font.PLAIN, 15));
		JLabel labelListeClientPC = new JLabel("Pour accèder à la liste des clients, cliquez ici:");
		labelListeClientPC.setFont(new Font("Times New Roman", Font.BOLD | Font.ITALIC, 20));

		// tailler et positionner les labels dans la page Client

		labelNouveauClientPC.setBounds(80, 30, 250, 30);
		labelNomPC.setBounds(100, 70, 250, 20);
		labelPrenomPC.setBounds(100, 95, 250, 20);
		labelDateNaissancePC.setBounds(100, 120, 250, 20);
		labelJourPC.setBounds(120, 145, 250, 20);
		labelMoisPC.setBounds(225, 145, 250, 20);
		labelAnneePC.setBounds(325, 145, 250, 20);
		labelAdressePC.setBounds(100, 170, 250, 20);
		labelNumRuePC.setBounds(120, 195, 250, 20);
		labelNomRuePC.setBounds(120, 220, 250, 20);
		labelNumLogementPC.setBounds(120, 245, 250, 20);
		labelCodePostalPC.setBounds(120, 270, 250, 20);
		labelVillePC.setBounds(120, 295, 250, 20);
		labelPhonePC.setBounds(100, 320, 250, 20);
		labelEmailPC.setBounds(100, 345, 250, 20);
		labelNumSecuPC.setBounds(100, 370, 250, 20);
		labelMutuellePC.setBounds(100, 395, 250, 20);
		labelMedecinPC.setBounds(100, 420, 250, 20);
		labelDejaClientPC.setBounds(550, 30, 250, 30);
		labelNomDejaClientPC.setBounds(570, 70, 250, 20);
		labelPrenomDejaClientPC.setBounds(570, 100, 250, 20);
		labelNumSecuDejaClientPC.setBounds(570, 130, 250, 20);
		labelListeClientPC.setBounds(550, 265, 418, 30);
		
		// Ajouter les labels dans le panel Client

		add(labelNouveauClientPC);
		add(labelNomPC);
		add(labelPrenomPC);
		add(labelDateNaissancePC);
		add(labelJourPC);
		add(labelMoisPC);
		add(labelAnneePC);
		add(labelAdressePC);
		add(labelNumRuePC);
		add(labelNomRuePC);
		add(labelNumLogementPC);
		add(labelCodePostalPC);
		add(labelVillePC);
		add(labelPhonePC);
		add(labelEmailPC);
		add(labelNumSecuPC);
		add(labelMutuellePC);
		add(labelMedecinPC);
		add(labelDejaClientPC);
		add(labelNomDejaClientPC);
		add(labelPrenomDejaClientPC);
		add(labelNumSecuDejaClientPC);
		add(labelListeClientPC);

		// les textfield pour les labels de la page Client

		JTextField tfNomPC = new JTextField ();
		JTextField tfPrenomPC = new JTextField ();
		JTextField tfJourPC = new JTextField ();
		tfJourPC.addKeyListener(new KeyAdapter() {

			public void keyTyped(KeyEvent e){
				char c = e.getKeyChar();
				if(!Character.isDigit(c)) {
					e.consume();
				}
			}
		});
		JTextField tfMoisPC = new JTextField ();
		tfMoisPC.addKeyListener(new KeyAdapter() {

			public void keyTyped(KeyEvent e){
				char c = e.getKeyChar();
				if(!Character.isDigit(c)) {
					e.consume();
				}
			}
		});
		JTextField tfAnneePC = new JTextField ();
		tfAnneePC.addKeyListener(new KeyAdapter() {

			public void keyTyped(KeyEvent e){
				char c = e.getKeyChar();
				if(!Character.isDigit(c)) {
					e.consume();
				}
			}
		});
		JTextField tfNumRuePC = new JTextField();
		tfNumRuePC.addKeyListener(new KeyAdapter() {

			public void keyTyped(KeyEvent e){
				char c = e.getKeyChar();
				if(!Character.isDigit(c)) {
					e.consume();
				}
			}
		});

		JTextField tfNomRuePC = new JTextField ();
		JTextField tfNumLogementPC = new JTextField ();
		tfNumLogementPC.addKeyListener(new KeyAdapter() {

			public void keyTyped(KeyEvent e){
				char c = e.getKeyChar();
				if(!Character.isDigit(c)) {
					e.consume();
				}
			}
		});

		JTextField tfCodePostalPC = new JTextField ();
		tfCodePostalPC.addKeyListener(new KeyAdapter() {

			public void keyTyped(KeyEvent e){
				char c = e.getKeyChar();
				if(!Character.isDigit(c)) {
					e.consume();
				}
			}
		});

		JTextField tfVillePC = new JTextField ();
		JTextField tfPhonePC = new JTextField ();
		JTextField tfEmailPC = new JTextField ();
		JTextField tfNumSecuPC = new JTextField ();
		tfNumSecuPC.addKeyListener(new KeyAdapter() {

			public void keyTyped(KeyEvent e){
				char c = e.getKeyChar();
				if(!Character.isDigit(c)) {
					e.consume();
				}
			}
		});
		JTextField tfMutuellePC = new JTextField ();
		JTextField tfMedecinPC = new JTextField ();
		JTextField tfNomDejaClientPC = new JTextField ();
		JTextField tfPrenomDejaClientPC = new JTextField ();
		JTextField tfNumSecuDejaClientPC = new JTextField ();
		tfNumSecuDejaClientPC.addKeyListener(new KeyAdapter() {

			public void keyTyped(KeyEvent e){
				char c = e.getKeyChar();
				if(!Character.isDigit(c)) {
					e.consume();
				}
			}
		});

		//Tailler et positionner les textfield pour la page client

		//nom
		tfNomPC.setBounds(270, 70, 150, 20);
		//prenom
		tfPrenomPC.setBounds(270, 95, 150, 20);
		//jour
		tfJourPC.setBounds(165, 145, 50, 20);
		//mois
		tfMoisPC.setBounds(265, 145, 50, 20);
		//anne
		tfAnneePC.setBounds(370, 145, 50, 20);
		//numero de la rue
		tfNumRuePC.setBounds(270, 195, 150, 20);
		//nom de la rue
		tfNomRuePC.setBounds(270, 220, 150, 20);
		//numero de logement
		tfNumLogementPC.setBounds(270, 245, 150, 20);
		//code postal
		tfCodePostalPC.setBounds(270, 270, 150, 20);
		//ville
		tfVillePC.setBounds(270, 295, 150, 20);
		//numero de téléphone
		tfPhonePC.setBounds(270, 320, 150, 20);
		//adresse mail
		tfEmailPC.setBounds(270, 345, 150, 20);
		//numero de la securité sociale
		tfNumSecuPC.setBounds(270, 370, 150, 20);
		//mutuelle
		tfMutuellePC.setBounds(270, 395, 150, 20);
		//medecin traitant
		tfMedecinPC.setBounds(270, 420, 150, 20);

		//nom à la partie déjà client
		tfNomDejaClientPC.setBounds(730, 70, 150, 20);
		//prénom à la partie déjà client
		tfPrenomDejaClientPC.setBounds(730, 100, 150, 20);
		//N° de la Sécurité Sociale à la partie déjà client
		tfNumSecuDejaClientPC.setBounds(730, 130, 150, 20);

		//Ajouter les texteField dans le panelClient

		add(tfNomPC);
		add(tfPrenomPC);
		add(tfJourPC);
		add(tfMoisPC);
		add(tfAnneePC);
		add(tfNumRuePC);
		add(tfNomRuePC);
		add(tfNumLogementPC);
		add(tfCodePostalPC);
		add(tfVillePC);
		add(tfPhonePC);
		add(tfEmailPC);
		add(tfNumSecuPC);
		add(tfMutuellePC);
		add(tfMedecinPC);
		add(tfNomDejaClientPC);
		add(tfPrenomDejaClientPC);
		add(tfNumSecuDejaClientPC);

		//Ajout des boutons dans la page client

		JButton buttonEffacerPC = new JButton("Effacer");
		JButton buttonEnregistrerPC = new JButton("Enregistrer");
		JButton buttonInfoClientPC = new JButton("Valider");
		JButton buttonListeClientPC = new JButton("Valider");
		JButton buttonQuitterPC = new JButton("Quitter");


		// Donner l'action au bouton Enregistrer 

		buttonEnregistrerPC.setFont(new Font("Times New Roman", Font.PLAIN, 13));
		buttonEnregistrerPC.setBounds(270, 490, 100, 30);
		buttonEnregistrerPC.setFocusable(false);
		buttonEnregistrerPC.setBackground(new Color(0xFFD700));
		buttonEnregistrerPC.addActionListener( e -> {

			long anneeCast = Long.parseLong(tfAnneePC.getText());
			int moisCast = Integer.parseInt(tfMoisPC.getText());
			int jourCast =Integer.parseInt(tfJourPC.getText());
			int numRueCast = Integer.parseInt(tfNumRuePC.getText());
			long numLogementCast = Long.parseLong(tfNumLogementPC.getText());
			long codePostalCast = Long.parseLong(tfCodePostalPC.getText());
			long phoneCast = Long.parseLong(tfPhonePC.getText());

			Dates date = new Dates(jourCast, moisCast, (int) anneeCast);

			Client client = null;
			try {
				client = new Client(tfNomPC.getText(), tfPrenomPC.getText(), date, tfNomRuePC.getText(), numRueCast, numLogementCast, codePostalCast, tfVillePC.getText(), phoneCast, tfEmailPC.getText(), tfNumSecuPC.getText(), tfMutuellePC.getText(), tfMedecinPC.getText());
			} catch (MissMatchException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			LesListes.addClient(client);

			// pour effacer les texteField après enregistrement

			tfNomPC.setText("");
			tfPrenomPC.setText("");
			tfJourPC.setText("");
			tfMoisPC.setText("");
			tfAnneePC.setText("");
			tfNumRuePC.setText("");
			tfNomRuePC.setText("");
			tfNumLogementPC.setText("");
			tfCodePostalPC.setText("");
			tfVillePC.setText("");
			tfPhonePC.setText("");
			tfEmailPC.setText("");
			tfNumSecuPC.setText("");
			tfMutuellePC.setText("");
			tfMedecinPC.setText("");

			for (int i=0; i<LesListes.LesClients.size();++i)
			{System.out.println(client.toString());}

		});


		//Donner l'action au bouton Effacer

		buttonEffacerPC.setFont(new Font("Times New Roman", Font.PLAIN, 13));
		buttonEffacerPC.setBounds(150, 490, 100, 30);
		buttonEffacerPC.setFocusable(false);
		buttonEffacerPC.setBackground(new Color(0xFFD700));
		buttonEffacerPC.addActionListener(e ->{

			tfNomPC.setText("");
			tfPrenomPC.setText("");
			tfJourPC.setText("");
			tfMoisPC.setText("");
			tfAnneePC.setText("");
			tfNumRuePC.setText("");
			tfNomRuePC.setText("");
			tfNumLogementPC.setText("");
			tfCodePostalPC.setText("");
			tfVillePC.setText("");
			tfPhonePC.setText("");
			tfEmailPC.setText("");
			tfNumSecuPC.setText("");
			tfMutuellePC.setText("");
			tfMedecinPC.setText("");
		});

		// Donner l'action au bouton qui nous amène à la fenetre Info client

		buttonInfoClientPC.setFont(new Font("Times New Roman", Font.PLAIN, 13));
		buttonInfoClientPC.setBounds(690, 180, 100, 30);
		buttonInfoClientPC.setFocusable(false);
		buttonInfoClientPC.setBackground(new Color(0xFFD700));
		buttonInfoClientPC.addActionListener(e ->{
			for (Client client : LesListes.getClient())	{

				String numSecu = tfNumSecuDejaClientPC.getText();
				String prenom = tfPrenomDejaClientPC.getText();
				String nom = tfNomDejaClientPC.getText();

				if (numSecu.equals(client.getNumSecuriteSociale()) || prenom.equals(client.getPrenom()) && nom.toUpperCase().equals(client.getNom())){
					new FenetreInfoClient();
					frame.dispose();
				}
				else if(numSecu.equals("") && prenom.equals("") && nom.toUpperCase().equals("")){
					JOptionPane.showMessageDialog(null, "Pour trouver un client, il faut:\nSoit le nom et prénom du client          \nSoit le N° Sécurité Sociale du client     \n ", "Attention!!!", JOptionPane.WARNING_MESSAGE);
				}
				else {
					JOptionPane.showMessageDialog(null, "Saisie Invalide\nVeuillez réessayer!!!    \n ", "Attention!!!", JOptionPane.WARNING_MESSAGE);
				}
				break;
			}
		});

		// Donner l'action pour le bouton qui amène à la fenêtre liste clients

		buttonListeClientPC.setFont(new Font("Times New Roman", Font.PLAIN, 13));
		buttonListeClientPC.setBounds(690, 330, 100, 30);
		buttonListeClientPC.setFocusable(false);
		buttonListeClientPC.setBackground(new Color(0xFFD700));
		buttonListeClientPC.addActionListener(e -> {
			
			new FenetreListeClients();
			frame.dispose();
			
		});

		// Donner l'action au bouton Quitter

		buttonQuitterPC.setFont(new Font("Garamond", Font.BOLD | Font.ITALIC, 18));
		buttonQuitterPC.setBorder(null);
		buttonQuitterPC.setBounds(800, 530, 150, 30);
		buttonQuitterPC.setFocusable(false);
		buttonQuitterPC.setBackground(null);
		buttonQuitterPC.addActionListener(e ->{
			System.exit(0);
		});

		add(buttonEffacerPC);
		add(buttonEnregistrerPC);
		add(buttonInfoClientPC);
		add(buttonListeClientPC);
		add(buttonQuitterPC);
		setLayout(null);
	}
}