
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.border.TitledBorder;
import javax.swing.border.EtchedBorder;



/**
 * 
 * @author User-06
 *
 */
class FenetreInfoClient extends JFrame {

	JFrame frame1 = new JFrame();
	JPanel panel;
	JTable table;
	JLabel Sparadrap, iconLabel, reponseNom, reponseNaissance, reponseNomRue, reponseNumLogement, reponseVille, reponsePhone, reponseMail, reponseSecu, reponseMutuelle, reponseMedecin;
	JButton buttonQuitter, buttonPagePrecedent, buttonPageAcceuil, bValider;
	final JComboBox<String> comboBox = new JComboBox<>();
	/**
	 * 
	 */
	FenetreInfoClient(){

		// icon de la fenêtre
		ImageIcon img = new ImageIcon("iconPharmacie.png");
		this.setTitle("Sparadrap");
		this.setIconImage(img.getImage());
		getContentPane().setLayout(null);
		this.getContentPane().setBackground(new Color(0xF4F7FC));
		panel = new JPanel();
		panel.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "Info de client", TitledBorder.LEFT, TitledBorder.TOP, null, Color.MAGENTA));

		//le label Sparadrap au dessus de la fenêtre
		Sparadrap = new JLabel("Sparadrap");
		Sparadrap.setBounds(350, 30, 300 ,60);
		Sparadrap.setFont(new Font("Georgia", Font.BOLD | Font.ITALIC, 52));
		Sparadrap.setForeground(Color.MAGENTA);

		//l'image de la fenêtre
		iconLabel = new JLabel();
		iconLabel.setIcon(img);
		iconLabel.setBounds(850, 0, 120, 120);


		//Ajouter un comboBox et y ajouter les noms des clients

		comboBox.setBounds(280, 68, 140, 20);
		comboBox.setEditable(true);
		comboBox.setFont(new Font("Times New Roman", Font.ITALIC, 14));
		comboBox.setBackground(new Color(0xFFD700));
		comboBox.addItem("Choisissez un nom");

		for (Client client : LesListes.getClient()){
			comboBox.addItem(client.getNom());
		}

		panel.add(comboBox);


		// organiser les textes / labels dans la page

		JLabel lTitre = new JLabel("Choisissez un client:");
		lTitre.setFont(new Font("Times New Roman", Font.BOLD | Font.ITALIC, 18));
		lTitre.setBounds(80, 60, 250, 30);

		JLabel dateNaissance = new JLabel("Date de naissance:");
		dateNaissance.setFont(new Font("Times New Roman", Font.PLAIN, 15));
		dateNaissance.setBounds(500, 160, 250, 20);
		dateNaissance.setVisible(false);

		JLabel adresse = new JLabel("Adresse:");
		adresse.setFont(new Font("Times New Roman", Font.PLAIN, 15));
		adresse.setBounds(500, 190, 250, 20);
		adresse.setVisible(false);

		JLabel numPhone = new JLabel("N° de téléphone:");
		numPhone.setFont(new Font("Times New Roman", Font.PLAIN, 15));
		numPhone.setBounds(500, 300, 250, 20);
		numPhone.setVisible(false);

		JLabel email = new JLabel("Adresse mail:");
		email.setFont(new Font("Times New Roman", Font.PLAIN, 15));
		email.setBounds(500, 330, 250, 20);
		email.setVisible(false);

		JLabel numSecu = new JLabel("N° de Sécurité Sociale:");
		numSecu.setFont(new Font("Times New Roman", Font.PLAIN, 15));
		numSecu.setBounds(500, 360, 250, 20);
		numSecu.setVisible(false);

		JLabel mutuelle = new JLabel("Mutuelle:");
		mutuelle.setFont(new Font("Times New Roman", Font.PLAIN, 15));
		mutuelle.setBounds(500, 390, 250, 20);
		mutuelle.setVisible(false);

		JLabel medecinTraitant = new JLabel("Médecin Traitant:");
		medecinTraitant.setFont(new Font("Times New Roman", Font.PLAIN, 15));
		medecinTraitant.setBounds(500, 420, 250, 20);
		medecinTraitant.setVisible(false);

		panel.add(lTitre);
		panel.add(dateNaissance);
		panel.add(adresse);
		panel.add(medecinTraitant);
		panel.add(numPhone);
		panel.add(email);
		panel.add(numSecu);
		panel.add(mutuelle);

		// Ajouter une place pour y mettre les infos des clients

		reponseNom = new JLabel();
		reponseNom.setFont(new Font("Times New Roman", Font.PLAIN | Font.ITALIC | Font.BOLD, 22));
		reponseNom.setBounds(570, 80, 300, 40);
		reponseNom.setForeground(Color.blue);

		reponseNaissance= new JLabel();
		reponseNaissance.setFont(new Font("Times New Roman", Font.PLAIN | Font.ITALIC, 15));
		reponseNaissance.setBounds(650, 160, 300, 20);
		reponseNaissance.setForeground(Color.blue);

		reponseNomRue= new JLabel();
		reponseNomRue.setFont(new Font("Times New Roman", Font.PLAIN | Font.ITALIC, 14));
		reponseNomRue.setBounds(650, 210, 300, 20);
		reponseNomRue.setForeground(Color.blue);

		reponseNumLogement= new JLabel();
		reponseNumLogement.setFont(new Font("Times New Roman", Font.PLAIN | Font.ITALIC, 14));
		reponseNumLogement.setBounds(650, 230, 300, 20);
		reponseNumLogement.setForeground(Color.blue);

		reponseVille= new JLabel();
		reponseVille.setFont(new Font("Times New Roman", Font.PLAIN | Font.ITALIC, 14));
		reponseVille.setBounds(650, 250, 300, 20);
		reponseVille.setForeground(Color.blue);

		reponsePhone= new JLabel();
		reponsePhone.setFont(new Font("Times New Roman", Font.PLAIN | Font.ITALIC, 15));
		reponsePhone.setBounds(650, 300, 300, 20);
		reponsePhone.setForeground(Color.blue);

		reponseMail= new JLabel();
		reponseMail.setFont(new Font("Times New Roman", Font.PLAIN | Font.ITALIC, 15));
		reponseMail.setBounds(650, 330, 300, 20);
		reponseMail.setForeground(Color.blue);

		reponseSecu= new JLabel();
		reponseSecu.setFont(new Font("Times New Roman", Font.PLAIN | Font.ITALIC, 15));
		reponseSecu.setBounds(650, 360, 300, 20);
		reponseSecu.setForeground(Color.blue);

		reponseMutuelle= new JLabel();
		reponseMutuelle.setFont(new Font("Times New Roman", Font.PLAIN | Font.ITALIC, 15));
		reponseMutuelle.setBounds(650, 390, 300, 20);
		reponseMutuelle.setForeground(Color.blue);

		reponseMedecin= new JLabel();
		reponseMedecin.setFont(new Font("Times New Roman", Font.PLAIN | Font.ITALIC, 15));
		reponseMedecin.setBounds(650, 420, 300, 20);
		reponseMedecin.setForeground(Color.blue);

		panel.add(reponseNom);
		panel.add(reponseNaissance);
		panel.add(reponseNomRue);
		panel.add(reponseNumLogement);
		panel.add(reponseVille);
		panel.add(reponsePhone);
		panel.add(reponseMail);
		panel.add(reponseSecu);
		panel.add(reponseMutuelle);
		panel.add(reponseMedecin);

		//le bouton pour sortir
		buttonQuitter = new JButton("Quitter");
		buttonQuitter.setBounds(800, 530, 100, 30);
		buttonQuitter.setFont(new Font("Garamond", Font.BOLD | Font.ITALIC, 18));
		buttonQuitter.setBorder(null);
		buttonQuitter.setBackground(null);
		buttonQuitter.setFocusable(false);
		buttonQuitter.addActionListener(e -> {
			System.exit(0);
		});
		panel.add(buttonQuitter);

		//le bouton pour la page d'acceuil

		buttonPageAcceuil = new JButton("Page d'acceuil");
		buttonPageAcceuil.setBounds(650, 530, 150, 30);
		buttonPageAcceuil.setFont(new Font("Garamond", Font.BOLD | Font.ITALIC, 18));
		buttonPageAcceuil.setBorder(null);
		buttonPageAcceuil.setBackground(null);
		buttonPageAcceuil.setFocusable(false);
		buttonPageAcceuil.addActionListener(e -> {
			new Main();
			dispose();
		});
		panel.add(buttonPageAcceuil);

		//Ajout d'un bouton pour la page précedent

		JButton buttonPagePrecedent = new JButton("< Page client");
		buttonPagePrecedent.setBounds(500, 530, 150, 30);
		buttonPagePrecedent.setFont(new Font("Garamond", Font.BOLD | Font.ITALIC, 18));
		buttonPagePrecedent.setBorder(null);
		buttonPagePrecedent.setBackground(null);
		buttonPagePrecedent.setFocusable(false);
		buttonPagePrecedent.addActionListener(e -> {

			new Main().tabbedPane.setSelectedIndex(1);
			dispose();

		});

		panel.add(buttonPagePrecedent);



		// Ajouter un bouton pour valider le choix et pour montrer l'info des clients

		bValider = new JButton("Valider");
		bValider.setFont(new Font("Times New Roman", Font.PLAIN, 13));
		bValider.setBounds(200, 150, 100, 30);
		bValider.setFocusable(false);
		bValider.setBackground(new Color(0xFFD700));


		bValider.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				String comboBoxItem = (String) comboBox.getSelectedItem();
				for (Client client : LesListes.getClient()){							
					if (comboBoxItem.equals(client.getNom())) {

						reponseNom.setText(client.getPrenom() + " " + client.getNom());
						reponseNaissance.setText(client.getDateDeNaissance().toString());
						reponseNomRue.setText(client.getNumRue() + " " + client.getNomRue());
						reponseNumLogement.setText("Logement N° " + client.getNumLogement());
						reponseVille.setText(client.getVille() + ", " + client.getCodePostal());
						reponsePhone.setText(client.getPhone() + "");
						reponseMail.setText(client.getMail());
						reponseSecu.setText(client.getNumSecuriteSociale());
						reponseMutuelle.setText(client.getMutuelle());
						reponseMedecin.setText(client.getMedecinTraitant());

						dateNaissance.setVisible(true);
						adresse.setVisible(true);
						numPhone.setVisible(true);
						email.setVisible(true);
						numSecu.setVisible(true);
						mutuelle.setVisible(true);
						medecinTraitant.setVisible(true);

					}
				}
			}

		});


		panel.add(bValider);
		panel.setBounds(0, 120, 985, 610);
		panel.setBackground(new Color(0XF5F5DC));
		panel.setLayout(null);


		getContentPane().add(panel);
		getContentPane().add(iconLabel);
		getContentPane().add(Sparadrap);
		getContentPane().setLayout(null);
		this.setSize(1000, 750);
		this.setResizable(false);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.setVisible(true);

	}
}