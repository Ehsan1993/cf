import java.time.LocalDate;
import java.util.ArrayList;
/**
 * 
 * @author User-06
 *
 */
public class Ordonnance {

	LocalDate dateOrdonnance;
	String medecinTraitant;
	String nomPatient;
	ArrayList<String> listeMedicaments;
	String nomSpecialiste;
	/**
	 * 
	 * @param dateOrdonnance
	 * @param medecinTraitant
	 * @param nomPatient
	 * @param listeMedicaments
	 * @param nomSpecialiste
	 */
	Ordonnance(LocalDate dateOrdonnance, String medecinTraitant, String nomPatient, ArrayList<String> listeMedicaments, String nomSpecialiste){
		this.dateOrdonnance = dateOrdonnance;
		this.medecinTraitant = medecinTraitant;
		this.nomPatient = nomPatient;
		this.listeMedicaments = listeMedicaments;
		this.nomSpecialiste = nomSpecialiste;

	}
	/**
	 * 
	 * @return
	 */
	public LocalDate getDateOrdonnance() {
		return dateOrdonnance;
	}
	/**
	 * 
	 * @param dateOrdonnance
	 */
	public void setDateOrdonnance(LocalDate dateOrdonnance) {
		this.dateOrdonnance = dateOrdonnance;
	}
	/**
	 * 
	 * @return
	 */
	public String getMedecinTraitant() {
		return medecinTraitant;
	}
	/**
	 * 
	 * @param medecinTraitant
	 */
	public void setMedecinTraitant(String medecinTraitant) {
		this.medecinTraitant = medecinTraitant;
	}
	/**
	 * 
	 * @return
	 */
	public String getNomPatient() {
		return nomPatient;
	}
	/**
	 * 
	 * @param nomPatient
	 */
	public void setNomPatient(String nomPatient) {
		this.nomPatient = nomPatient;
	}
	/**
	 * 
	 * @return
	 */
	public ArrayList<String> getListeMedicaments() {
		return listeMedicaments;
	}
	/**
	 * 
	 * @param listeMedicaments
	 */
	public void setListeMedicaments(ArrayList<String> listeMedicaments) {
		this.listeMedicaments = listeMedicaments;
	}
	/**
	 * 
	 * @return
	 */
	public String getNomSpecialiste() {
		return nomSpecialiste;
	}
	/**
	 * 
	 * @param nomSpecialiste
	 */
	public void setNomSpecialiste(String nomSpecialiste) {
		this.nomSpecialiste = nomSpecialiste;
	}
}


