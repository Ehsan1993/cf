/**
 * 
 * @author User-06
 *
 */
public class Specialiste extends Personne{

	String specialite;
	/**
	 * 
	 * @param nom
	 * @param prenom
	 * @param nomRue
	 * @param numRue
	 * @param numLogement
	 * @param codePostal
	 * @param ville
	 * @param phone
	 * @param mail
	 * @param specialite
	 */
	Specialiste(String nom, String prenom, String nomRue, int numRue, long numLogement, long codePostal, String ville,
			long phone, String mail, String specialite) {
		super(nom, prenom, nomRue, numRue, numLogement, codePostal, ville, phone, mail);
		this.specialite = specialite;
	}
	/**
	 * 
	 * @return
	 */
	public String getSpecialite() {
		return specialite;
	}
	/**
	 * 
	 * @param specialite
	 */
	public void setSpecialite(String specialite) {
		this.specialite = specialite;
	}
	/**
	 * 
	 */
	public String toString() {
		return "Dr. " + this.getNom() + " " + this.getPrenom();
	}
}

