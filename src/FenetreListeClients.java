
import java.awt.Color;
import java.awt.Font;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.border.EtchedBorder;
import javax.swing.border.BevelBorder;
import javax.swing.ScrollPaneConstants;
/**
 * 
 * @author frame pour la fenêtre listeClients
 *
 */
public class FenetreListeClients extends JFrame {

	JFrame frame1 = new JFrame();
	JPanel panel = new JPanel();
	static JTable table;
	JLabel Sparadrap, iconLabel, ListClientLabel, teteTable;
	JButton buttonQuitter, buttonPagePrecedent, buttonPageAcceuil;
	/**
	 * 	la fenetre listeClients
	 */
	FenetreListeClients(){

		// icon de la fenêtre

		ImageIcon img = new ImageIcon("iconPharmacie.png");
		this.setTitle("Sparadrap");
		this.setIconImage(img.getImage());
		getContentPane().setLayout(null);
		this.getContentPane().setBackground(new Color(0xF4F7FC));
		panel.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "LISTE DES CLIENTS", TitledBorder.CENTER, TitledBorder.TOP, null, Color.MAGENTA));

		//le label Sparadrap au dessus de la fenêtre

		Sparadrap = new JLabel("Sparadrap");
		Sparadrap.setBounds(350, 30, 300 ,60);
		Sparadrap.setFont(new Font("Georgia", Font.BOLD | Font.ITALIC, 52));
		Sparadrap.setForeground(Color.MAGENTA);

		//l'image de la fenêtre

		iconLabel = new JLabel();
		iconLabel.setIcon(img);
		iconLabel.setBounds(850, 0, 120, 120);

		String header [] = {"Nom", "Prenom", "Date de naissance", "Telephone", "Email", "N°Securité Sociale", "Medecin traitant"};
		JTable table = new JTable();

		DefaultTableModel model= new DefaultTableModel(header, 0);
		table.setModel(model);
		Object rowData[] = new Object[7];

		for (Client client: LesListes.LesClients) {
			rowData[0] = client.getNom();
			rowData[1] = client.getPrenom();
			rowData[2] = client.getDateDeNaissance();
			rowData[3] = client.getPhone();
			rowData[4] = client.getMail();
			rowData[5] = client.getNumSecuriteSociale();
			rowData[6] = client.getMedecinTraitant();
			model.addRow(rowData);
		}

		table.setEnabled(false);
		table.setRowSelectionAllowed(true);
		table.setSurrendersFocusOnKeystroke(true);
		table.setFont(new Font("Times New Roman", Font.PLAIN, 13));
		table.setColumnSelectionAllowed(true);
		table.setCellSelectionEnabled(true);

		table.getColumnModel().getColumn(0).setResizable(false);
		table.getColumnModel().getColumn(0).setPreferredWidth(80);
		table.getColumnModel().getColumn(0).setMinWidth(80);
		table.getColumnModel().getColumn(1).setPreferredWidth(80);
		table.getColumnModel().getColumn(1).setMinWidth(80);
		table.getColumnModel().getColumn(2).setPreferredWidth(100);
		table.getColumnModel().getColumn(2).setMinWidth(80);
		table.getColumnModel().getColumn(3).setPreferredWidth(80);
		table.getColumnModel().getColumn(3).setMinWidth(80);
		table.getColumnModel().getColumn(4).setPreferredWidth(130);
		table.getColumnModel().getColumn(4).setMinWidth(130);
		table.getColumnModel().getColumn(5).setPreferredWidth(100);
		table.getColumnModel().getColumn(5).setMinWidth(80);
		table.getColumnModel().getColumn(6).setPreferredWidth(120);
		table.getColumnModel().getColumn(6).setMinWidth(100);

		table.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));

		JTableHeader header1 = table.getTableHeader();
		header1.setBackground(new Color(0xFFD700));

		// Ajout de Scroll pour la liste dans la fenêtre de la liste des clients

		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setBounds(21, 21, 943, 488);
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setToolTipText("");
		scrollPane.setViewportBorder(new BevelBorder(BevelBorder.RAISED, Color.LIGHT_GRAY, null, null, null));

		//le bouton pour sortir
		buttonQuitter = new JButton("Quitter");
		buttonQuitter.setBounds(800, 530, 100, 30);
		buttonQuitter.setFont(new Font("Garamond", Font.BOLD | Font.ITALIC, 18));
		buttonQuitter.setBorder(null);
		buttonQuitter.setBackground(null);
		panel.add(buttonQuitter);
		buttonQuitter.setFocusable(false);
		buttonQuitter.addActionListener(e -> {
			System.exit(0);
		});

		//le bouton pour le page d'acceuil

		buttonPageAcceuil = new JButton("Page d'acceuil");
		buttonPageAcceuil.setBounds(650, 530, 150, 30);
		buttonPageAcceuil.setFont(new Font("Garamond", Font.BOLD | Font.ITALIC, 18));
		buttonPageAcceuil.setBorder(null);
		buttonPageAcceuil.setBackground(null);
		panel.add(buttonPageAcceuil);
		buttonPageAcceuil.setFocusable(false);
		buttonPageAcceuil.addActionListener(e -> {
			new Main();
			dispose();
		});

		//Ajout le bouton pour la page précedent

		JButton buttonPagePrecedent = new JButton("< Page client");
		buttonPagePrecedent.setBounds(500, 530, 150, 30);
		buttonPagePrecedent.setFont(new Font("Garamond", Font.BOLD | Font.ITALIC, 18));
		buttonPagePrecedent.setBorder(null);
		buttonPagePrecedent.setBackground(null);
		buttonPagePrecedent.setFocusable(false);
		buttonPagePrecedent.addActionListener(e -> {

			new Main().tabbedPane.setSelectedIndex(1);
			dispose();

		});

		panel.add(buttonPagePrecedent);




		panel.setBounds(0, 120, 985, 610);
		panel.setBackground(new Color(0XF5F5DC));
		panel.setLayout(null);
		panel.add(scrollPane);


		getContentPane().add(panel);
		getContentPane().add(iconLabel);
		getContentPane().add(Sparadrap);
		getContentPane().setLayout(null);
		this.setSize(1000, 750);
		this.setResizable(false);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.setVisible(true);

	}
}