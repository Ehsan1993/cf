import java.util.regex.Pattern;

import exceptions.MissMatchException;

/**
 * 
 * @author User-06
 *
 */
public class Client extends Personne{
	Dates dateDeNaissance;
	String numSecuriteSociale;
	String mutuelle;
	String medecinTraitant;
	/**
	 * 
	 * @param nom
	 * @param prenom
	 * @param dateNaissanceClient
	 * @param nomRue
	 * @param numRue
	 * @param numLogement
	 * @param codePostal
	 * @param ville
	 * @param phone
	 * @param mail
	 * @param numSecuriteSociale
	 * @param mutuelle
	 * @param medecinTraitant
	 * @throws MissMatchException 
	 */
	Client(String nom, String prenom, Dates dateNaissanceClient, String nomRue, int numRue, long numLogement, long codePostal, String ville, long phone,
			String mail, String numSecuriteSociale, String mutuelle, String medecinTraitant) throws MissMatchException {
		super(nom, prenom, nomRue, numRue, numLogement, codePostal, ville, phone, mail);
		this.setDateDeNaissance(dateNaissanceClient);
		this.setNumSecuriteSociale(numSecuriteSociale);
		this.setMutuelle(mutuelle);
		this.setMedecinTraitant(medecinTraitant);
	}
	/**
	 * 
	 * @return
	 */
	public Dates getDateDeNaissance() {
		return dateDeNaissance;
	}
	/**
	 * 
	 * @param dateDeNaissance
	 */
	public void setDateDeNaissance(Dates dateDeNaissance) {
		this.dateDeNaissance = dateDeNaissance;
	}
	/**
	 * 
	 * @return
	 */
	public String getNumSecuriteSociale() {
		return numSecuriteSociale;
	}
	/**
	 * 
	 * @param numSecuriteSociale
	 * @throws MissMatchException 
	 */
	public void setNumSecuriteSociale(String numSecuriteSociale) throws MissMatchException {
		if (!Pattern.matches("[0-9]{15}", numSecuriteSociale)) throw new MissMatchException("Un numero de securite sociale contient forcement 11 chiffres"); 
		this.numSecuriteSociale = numSecuriteSociale;
	}
	/**
	 * 
	 * @return
	 */
	public String getMutuelle() {
		return mutuelle;
	}
	/**
	 * 
	 * @param mutuelle
	 */
	public void setMutuelle(String mutuelle) {
		this.mutuelle = mutuelle;
	}
	/**
	 * 
	 * @return
	 */
	public String getMedecinTraitant() {
		return medecinTraitant;
	}
	/**
	 * 
	 * @param medecinTraitant
	 */
	public void setMedecinTraitant(String medecinTraitant) {
		this.medecinTraitant = medecinTraitant;
	}
	/**
	 * 
	 */
	@Override
	public String toString() {
		return this.getNom()+" "+this.getPrenom();
	}
}