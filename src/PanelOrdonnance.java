import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
/**
 * 
 * @author User-06
 *
 */
public class PanelOrdonnance extends JPanel {
	/**
	 * 
	 * @param Organisation de la page Ordonnance
	 */
	PanelOrdonnance(JFrame frame){

		setBackground(new Color(0XF5F5DC));

		//Ajout des labels/Textes

		JLabel lHistoriqueOrdonnance = new JLabel("Historique des ordonnances");
		lHistoriqueOrdonnance.setBounds(370, 30, 300, 40);
		lHistoriqueOrdonnance.setFont(new Font("Times New Roman", Font.BOLD | Font.ITALIC, 22));


		JLabel lOrdonnance = new JLabel("Les ordonnances de:");
		lOrdonnance.setBounds(50, 200, 250, 30);
		lOrdonnance.setFont(new Font("Times New Roman", Font.ITALIC, 18));


		JLabel lNomMedecin = new JLabel("Nom de médecin:");
		lNomMedecin.setBounds(70, 250, 150, 30);
		lNomMedecin.setFont(new Font("Times New Roman", Font.PLAIN, 15));


		JComboBox<Medecin> cbNomMedecin = new JComboBox<> ();
		cbNomMedecin.setBounds(200, 258, 200, 20);
		cbNomMedecin.setFocusable(false);
		cbNomMedecin.setFont(new Font("Times New Roman", Font.ITALIC, 14));
		cbNomMedecin.setBackground(new Color(0xd9c0d5));

		for (Medecin medecin : LesListes.getMedecin()){
			cbNomMedecin.addItem(medecin);
		}
	


		// Ajout et donner l'action au bouton pour consulter les ordonnances


		JButton bOrdonnances = new JButton("Accèder aux ordonnances");
		bOrdonnances.setBounds(0, 530, 800, 50);
		bOrdonnances.setFont(new Font("Times New Roman", Font.ITALIC| Font.BOLD, 18));
		bOrdonnances.setFocusable(false);
		bOrdonnances.setBorder(null);
		bOrdonnances.setBackground(new Color(0xd9c0d5));
		bOrdonnances.addActionListener(e ->{
			new FenetreListeOrdonnance();
			frame.dispose();
		});

		add(lHistoriqueOrdonnance);
		add(lOrdonnance);
		add(lNomMedecin);
		add(cbNomMedecin);
		add(bOrdonnances);


		// Ajout d'un bouton pour valider d'aller à liste des ordonnances

		JButton bValiderOrdonnance = new JButton("Valider");
		bValiderOrdonnance.setBounds(150, 300, 100, 30);
		bValiderOrdonnance.setFont(new Font("Times New Roman", Font.PLAIN, 13));
		bValiderOrdonnance.setFocusable(false);
		bValiderOrdonnance.setBackground(new Color(0xFFD700));
		bValiderOrdonnance.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {


				String nomMedecin = (String) cbNomMedecin.getSelectedItem();
				for (Medecin medecin : LesListes.getMedecin()) {
					if (nomMedecin.equals("Dr. " + medecin.getNom() + " " + medecin.getPrenom())){
						JOptionPane.showMessageDialog(null, "Ce n'est pas encore fini. :)");
					}
				}
			}
		});

		add(bValiderOrdonnance);

		// Donner l'action au bouton Quitter

		JButton buttonSortirOrdonnance = new JButton("Quitter");		
		buttonSortirOrdonnance.setFont(new Font("Garamond", Font.BOLD | Font.ITALIC, 18));
		buttonSortirOrdonnance.setBorder(null);
		buttonSortirOrdonnance.setBounds(800, 530, 150, 30);
		buttonSortirOrdonnance.setFocusable(false);
		buttonSortirOrdonnance.setBackground(null);
		buttonSortirOrdonnance.addActionListener(e ->{
			System.exit(0);
		});

		add(buttonSortirOrdonnance);



		setLayout(null);

	}
}
